import {Component, Input} from '@angular/core';
import {Setting} from '../setting';
import {SettingType} from '../setting-type';
import {SettingsService} from '../settings.service';

@Component({
  selector: 'app-settings-item',
  templateUrl: './settings-item.component.html',
  styleUrls: ['./settings-item.component.css']
})
export class SettingsItemComponent {

  @Input() setting: Setting;
  SettingType = SettingType;

  constructor(private settingsService: SettingsService) {}

  setSetting(value: any) {
    this.settingsService.setSettings(this.setting.key, value);
  }

  getSetting() {
    return this.settingsService.getSettings(this.setting.key);
  }
}
