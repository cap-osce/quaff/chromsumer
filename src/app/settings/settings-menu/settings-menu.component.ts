import {Component, HostListener, OnInit} from '@angular/core';
import { ClockSettingsList} from '../../components/misc/clock/clock.settings';
import {Setting} from '../setting';
import {ServerStatusSettingsList} from '../../components/status/server/server-status.settings';

@Component({
  selector: 'app-settings-menu',
  templateUrl: './settings-menu.component.html',
  styleUrls: ['./settings-menu.component.css']
})
export class SettingsMenuComponent implements OnInit {
  menuActive = false;
  clockSettings: Setting[] = ClockSettingsList;
  statusSettings: Setting[] = ServerStatusSettingsList;

  constructor() {}

  ngOnInit() {
  }

  toggleMenu() {
    this.menuActive = !this.menuActive;
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(evt: KeyboardEvent) {
    this.menuActive = false;
  }
}
