import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {}

  isSet(key: string) {
    return this.storage.get(key) || this.storage.get(key) === false;
  }

  setSettings(key: string, value: any) {
    this.storage.set(key, value);
  }

  getSettings(key: string): any {
    return this.storage.get(key);
  }
}
