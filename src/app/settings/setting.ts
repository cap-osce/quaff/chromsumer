import {SettingType} from './setting-type';
import {SettingOption} from './setting-option';

export class Setting {
  key: string;
  type: SettingType;
  label: string;
  options: SettingOption[] = [];

  constructor(setting) {
    this.key = setting.key;
    this.type = setting.type;
    this.label = setting.label;
    this.options = setting.options;
  }
}
