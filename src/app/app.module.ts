import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ClockComponent } from './components/misc/clock/clock.component';
import {StorageServiceModule} from 'ngx-webstorage-service';
import { SettingsMenuComponent } from './settings/settings-menu/settings-menu.component';
import { SettingsItemComponent } from './settings/settings-item/settings-item.component';
import { ServerStatusComponent } from './components/status/server/server-status.component';
import { DraggableComponent } from './components/misc/draggable/draggable.component';
import {AngularDraggableModule} from 'angular2-draggable';
import { TodoComponent } from './components/todo/todo.component';
import {HttpClientModule} from '@angular/common/http';
import { JiraComponent } from './components/status/jira/jira.component';
import { JenkinsComponent } from './components/status/jenkins/jenkins.component';

@NgModule({
  declarations: [
    AppComponent,
    ClockComponent,
    SettingsMenuComponent,
    SettingsItemComponent,
    ServerStatusComponent,
    DraggableComponent,
    TodoComponent,
    JiraComponent,
    JenkinsComponent
  ],
  imports: [
    BrowserModule,
    StorageServiceModule,
    AngularDraggableModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
