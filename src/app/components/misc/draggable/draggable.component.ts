import {Component, Input, OnInit} from '@angular/core';
import {SettingsService} from '../../../settings/settings.service';

@Component({
  selector: 'app-draggable',
  templateUrl: './draggable.component.html',
  styleUrls: ['./draggable.component.css']
})
export class DraggableComponent implements OnInit {
  static POSITION_KEY = '_stored_panel_position';

  @Input() position: any;
  @Input() positionStorageKeyPrefix: string;
  @Input() ghostMode: boolean;

  positionStorageKey: string;
  hovered: boolean;

  constructor(private settingsService: SettingsService) {}

  ngOnInit() {
    if (this.positionStorageKeyPrefix) {
      this.positionStorageKey = this.positionStorageKeyPrefix + DraggableComponent.POSITION_KEY;
      if (!this.settingsService.isSet(this.positionStorageKey)) {
        this.settingsService.setSettings(this.positionStorageKey + DraggableComponent.POSITION_KEY, { x: 25, y: 25 });
      }
      this.position = this.settingsService.getSettings(this.positionStorageKey);
    }
  }

  cardDroppedEvent($event) {
    if (this.positionStorageKeyPrefix) {
      this.settingsService.setSettings(this.positionStorageKey, $event);
    }
  }

  mouseEnter() {
    this.hovered = true;
  }

  mouseLeave() {
    this.hovered = false;
  }
}
