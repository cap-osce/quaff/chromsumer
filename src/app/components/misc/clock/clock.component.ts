import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import {BehaviorSubject} from 'rxjs';
import {SettingsService} from '../../../settings/settings.service';
import {ClockSettings} from './clock.settings';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {
  static FORMAT_24 = 'HH:mm';
  static FORMAT_12 = 'h:mm a';

  theTime: BehaviorSubject<string> = new BehaviorSubject<string>('');
  theDate: BehaviorSubject<string> = new BehaviorSubject<string>('');
  is24Hour: boolean;
  settings = ClockSettings;

  constructor(private settingsService: SettingsService) {
    if (!this.settingsService.isSet(ClockSettings.IS_24_HOUR)) {
      this.settingsService.setSettings(ClockSettings.IS_24_HOUR, true);
    }
    if (!this.settingsService.isSet(ClockSettings.VERTICAL_POSITION)) {
      this.settingsService.setSettings(ClockSettings.VERTICAL_POSITION, ClockSettings.POSITION_MID);
    }
    if (!this.settingsService.isSet(ClockSettings.HORIZONTAL_POSITION)) {
      this.settingsService.setSettings(ClockSettings.HORIZONTAL_POSITION, ClockSettings.POSITION_MID);
    }
  }

  ngOnInit() {
    this.updateTheTime();
    this.updateTheDate();
    setInterval(() => this.updateTheTime(), 1000);
    setInterval(() => this.updateTheDate(), 60000);
  }

  updateTheTime() {
    this.is24Hour = this.settingsService.getSettings(ClockSettings.IS_24_HOUR);
    this.theTime.next(moment().format(this.is24Hour ? ClockComponent.FORMAT_24 : ClockComponent.FORMAT_12));
  }

  updateTheDate() {
    this.theDate.next(moment().format('MMMM Do'));
  }

  getVerticalPosition() {
    return this.settingsService.getSettings(ClockSettings.VERTICAL_POSITION);
  }

  getHorizontalPosition() {
    return this.settingsService.getSettings(ClockSettings.HORIZONTAL_POSITION);
  }
}
