import {Setting} from '../../../settings/setting';
import {SettingType} from '../../../settings/setting-type';
import {SettingOption} from '../../../settings/setting-option';

export class ClockSettings {
  static IS_24_HOUR = 'clock_is_twenty_four_hour';

  static VERTICAL_POSITION = 'clock_vertical_position';
  static HORIZONTAL_POSITION = 'clock_horizontal_position';

  static POSITION_TOP = 'top';
  static POSITION_MID = 'mid';
  static POSITION_BOT = 'bot';
}

export const ClockSettingsList = [
  new Setting({
    key: ClockSettings.IS_24_HOUR,
    type: SettingType.BOOLEAN,
    label: 'Format as 24 hours?'
  }),
  new Setting({
    key: ClockSettings.VERTICAL_POSITION,
    type: SettingType.OPTION,
    label: 'Vertical Position',
    options: [
      new SettingOption('Top', ClockSettings.POSITION_TOP),
      new SettingOption('Middle', ClockSettings.POSITION_MID),
      new SettingOption('Bottom', ClockSettings.POSITION_BOT)
    ]
  }),
  new Setting({
    key: ClockSettings.HORIZONTAL_POSITION,
    type: SettingType.OPTION,
    label: 'Horizontal Position',
    options: [
      new SettingOption('Left', ClockSettings.POSITION_TOP),
      new SettingOption('Middle', ClockSettings.POSITION_MID),
      new SettingOption('Right', ClockSettings.POSITION_BOT)
    ]
  })
];
