import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  names = ['jon', 'bill', 'bob', 'mike'];

   todolist = [
    {prority:  1, text: 'Renew server certificate'},
    {priority: 2, text: 'Book conference travel for team'},
    {priority: 1, text: 'Book Hotel in London for Thursday and Friday'},
    {priority: 1, text: 'Set up interview for new Business Analyst'},
    {priority: 4, text: 'Book flight to Germany on the 22nd January'}
  ];


  constructor() {
  }


  ngOnInit() {
  }

}
