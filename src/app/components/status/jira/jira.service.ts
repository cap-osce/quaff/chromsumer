import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';
import {Deserialize} from 'cerialize';
import {JiraProjects} from './jira-projects';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JiraService {

  constructor(private http: HttpClient) { }

  getSprints(): Observable<JiraProjects> {
    return this.http.get('http://quaffacade.quaff.harmelodic.com/quaff/jira').pipe(
      map((data) => {
        return Deserialize(data, JiraProjects);
      })
    );
  }
}
