import {autoserializeAs} from 'cerialize';
import {JiraProject} from './jira-project';

export class JiraProjects {
  @autoserializeAs(JiraProject) projects: JiraProject[];
}
