import {autoserialize} from 'cerialize';

export class JiraSprint {
  @autoserialize id: string;
  @autoserialize status: string;
}
