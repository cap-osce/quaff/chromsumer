import { Component, OnInit } from '@angular/core';
import {interval, Observable} from 'rxjs';
import {startWith, switchMap} from 'rxjs/operators';
import {JiraService} from './jira.service';

@Component({
  selector: 'app-jira',
  templateUrl: './jira.component.html',
  styleUrls: ['./jira.component.css', '../status.css']
})
export class JiraComponent implements OnInit {

  statuses: Observable<any>;

  constructor(private jiraService: JiraService) { }

  ngOnInit() {
    this.statuses = interval(1000 * 5).pipe(
      startWith(0),
      switchMap(() => this.jiraService.getSprints())
    );
  }

  getProjects(): Observable<any> {
    return this.statuses;
  }
}
