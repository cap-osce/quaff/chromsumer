import {autoserialize, autoserializeAs} from 'cerialize';
import {JiraSprint} from './jira-sprint';

export class JiraProject {
  @autoserialize projectId: string;
  @autoserializeAs(JiraSprint) sprints: JiraSprint[];
}
