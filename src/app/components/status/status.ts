import {autoserialize} from 'cerialize';

export class Status {
  @autoserialize id: string;
  @autoserialize status: string;
}
