import {autoserialize} from 'cerialize';

export class JenkinsBuild {
  @autoserialize name: string;
  @autoserialize colour: string;
  @autoserialize url: string;
}
