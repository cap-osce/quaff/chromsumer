import { Injectable } from '@angular/core';
import {JiraProjects} from '../jira/jira-projects';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';
import {Deserialize} from 'cerialize';
import {JenkinsBuilds} from './jenkins-builds';

@Injectable({
  providedIn: 'root'
})
export class JenkinsService {

  constructor(private http: HttpClient) { }

  getBuilds(): Observable<JenkinsBuilds> {
    return this.http.get('http://quaffacade.quaff.harmelodic.com/quaff/jenkins').pipe(
      map((data) => {
        return Deserialize(data, JenkinsBuilds);
      })
    );
  }
}
