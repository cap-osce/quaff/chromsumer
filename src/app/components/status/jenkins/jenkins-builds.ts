import {autoserializeAs} from 'cerialize';
import {JenkinsBuild} from './jenkins-build';

export class JenkinsBuilds {
  @autoserializeAs(JenkinsBuild) builds: JenkinsBuild;
}
