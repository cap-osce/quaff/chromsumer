import { Component, OnInit } from '@angular/core';
import {startWith, switchMap} from 'rxjs/operators';
import {interval, Observable} from 'rxjs';
import {ServerStatuses} from '../server/server-statuses';
import {JenkinsService} from './jenkins.service';
import {JenkinsBuilds} from './jenkins-builds';

@Component({
  selector: 'app-jenkins',
  templateUrl: './jenkins.component.html',
  styleUrls: ['./jenkins.component.css', '../status.css']
})
export class JenkinsComponent implements OnInit {
  builds: Observable<JenkinsBuilds>;

  constructor(private jenkinsService: JenkinsService) { }

  ngOnInit() {
    this.builds = interval(1000 * 5).pipe(
      startWith(0),
      switchMap(() => this.jenkinsService.getBuilds())
    );
  }

  getBuilds(): Observable<JenkinsBuilds> {
    return this.builds;
  }

}
