import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Deserialize} from 'cerialize';
import {ServerStatuses} from './server-statuses';

@Injectable({
  providedIn: 'root'
})
export class ServerStatusService {

  constructor(private http: HttpClient) { }

  getStatuses() {
    return this.http.get('http://quaffacade.quaff.harmelodic.com/quaff/kubernetes/quaff-dev').pipe(
      map((data) => Deserialize(data, ServerStatuses))
    );
  }
}
