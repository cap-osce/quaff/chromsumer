import { autoserializeAs} from 'cerialize';
import {Status} from '../status';

export class ServerStatuses {
  @autoserializeAs(Status) tickets: Status;
}
