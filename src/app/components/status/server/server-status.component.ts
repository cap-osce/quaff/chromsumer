import { Component, OnInit } from '@angular/core';
import {SettingsService} from '../../../settings/settings.service';
import {interval, Observable} from 'rxjs';
import {ServerStatusService} from './server-status.service';
import {switchMap} from 'rxjs/operators';
import {startWith} from 'rxjs/operators';
import {ServerStatusSettings} from './server-status.settings';
import {ServerStatuses} from './server-statuses';

@Component({
  selector: 'app-server-status',
  templateUrl: './server-status.component.html',
  styleUrls: ['./server-status.component.css', '../status.css']
})
export class ServerStatusComponent implements OnInit {

  position: any;
  statuses: Observable<ServerStatuses>;
  settings = ServerStatusSettings;

  constructor(private settingsService: SettingsService, private serverStatusService: ServerStatusService) {
    if (!this.settingsService.isSet(ServerStatusSettings.LAYOUT)) {
      this.settingsService.setSettings(ServerStatusSettings.LAYOUT, ServerStatusSettings.VERTICAL);
    }
  }

  ngOnInit() {
    this.statuses = interval(1000 * 5).pipe(
      startWith(0),
      switchMap(() => this.serverStatusService.getStatuses())
    );
  }

  getStatuses(): Observable<ServerStatuses> {
    return this.statuses;
  }

  getLayout() {
    return this.settingsService.getSettings(ServerStatusSettings.LAYOUT);
  }
}
