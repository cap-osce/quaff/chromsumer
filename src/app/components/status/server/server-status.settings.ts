import {Setting} from '../../../settings/setting';
import {SettingType} from '../../../settings/setting-type';
import {SettingOption} from '../../../settings/setting-option';

export class ServerStatusSettings {
  static LAYOUT = 'server_status_layout';

  static VERTICAL = 'vertical';
  static HORIZONTAL = 'horizontal';
}

export const ServerStatusSettingsList = [
  new Setting({
    key: ServerStatusSettings.LAYOUT,
    type: SettingType.OPTION,
    label: 'Layout',
    options: [
      new SettingOption('Horizontal', ServerStatusSettings.HORIZONTAL),
      new SettingOption('Vertical', ServerStatusSettings.VERTICAL)
    ]
  })
];

